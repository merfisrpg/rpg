package sk.perri.rpg.area;

public enum AreaType
{
    VILLAGE, DUNGEON, SPAWN, SPAWNPOINT
}
