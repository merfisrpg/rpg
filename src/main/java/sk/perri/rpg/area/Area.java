package sk.perri.rpg.area;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import sk.perri.rpg.Holder;
import sk.perri.rpg.Utils;
import sk.perri.rpg.classes.Monster;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Area
{
    private Location l1, l2, checkpoint;
    private String name;
    private String displayName;
    private Map<Monster, Integer> monsters = new HashMap<>();
    private boolean showTitle = false;
    private AreaType at;

    public Area(String name, ConfigurationSection cs)
    {
        this.name = name;
        at = AreaType.valueOf(cs.getString("areatype"));
        l1 = Utils.parseLocation(cs.getConfigurationSection("l1"));
        l2 = cs.isSet("l2") ? Utils.parseLocation(cs.getConfigurationSection("l2")) : null;
        checkpoint = cs.isSet("checkpoint") ? Utils.parseLocation(cs.getConfigurationSection("checkpoint")) : null;
        displayName = cs.isSet("name") ? ChatColor.translateAlternateColorCodes('&', cs.getString("name")) : name;
        showTitle = cs.isSet("showname") && cs.getBoolean("showname");

        if(cs.isSet("monsters"))
        {
            cs.getStringList("monsters").forEach(l ->
            {
                String[] s = l.split(";");
                Monster m = Holder.monsters.get(s[0].toLowerCase());
                if(m == null)
                    Bukkit.getLogger().info("Unable to get monster: "+s[0]);
                else
                    monsters.put(m, Integer.parseInt(s[1]));
            });
        }
    }

    public boolean isIn(Location loc, boolean ignoreY)
    {
        if(l2 == null)
            return false;

        int minX = Math.min(l1.getBlockX(), l2.getBlockX());
        int maxX = Math.max(l1.getBlockX(), l2.getBlockX());

        int minZ = Math.min(l1.getBlockZ(), l2.getBlockZ());
        int maxZ = Math.max(l1.getBlockZ(), l2.getBlockZ());

        int minY = Math.min(l1.getBlockY(), l2.getBlockY());
        int maxY = Math.max(l1.getBlockY(), l2.getBlockY());

        return minX <= loc.getX() && loc.getX() <= maxX && minZ <= loc.getZ() && loc.getZ() <= maxZ &&
                (ignoreY || (minY <= loc.getY() && loc.getY() <= maxY));
    }

    public boolean showTitle() { return showTitle; }
    public String getTitle() { return displayName; }
    public AreaType getAreaType() { return at; }

    public void spawnMonsters()
    {
        Map<Monster, Integer> toSpawn = new HashMap<>();

        monsters.forEach((m, t) ->
        {
            final int[] toSp = {t};

            if(m == null)
            {
                Bukkit.getLogger().info("Monster is null");
                return;
            }

            //Bukkit.getLogger().info("Checking: "+m.getName()+" - "+t);

            Collection<Entity> le = l1.getWorld().getNearbyEntities(l1, 1, 1, 1);
            le.forEach(ee ->
            {
                //Bukkit.getLogger().info("Entity: "+ee.getCustomName());
                if(ee.getCustomName().equalsIgnoreCase(m.getName()))
                {
                    toSp[0]--;
                    //Bukkit.getLogger().info("toSp-- new: "+toSp[0]);
                }
            });

            if(toSp[0] > 0)
            {
                toSpawn.put(m, toSp[0]);
                //Bukkit.getLogger().info("adding to spawn: "+m.getName()+" - "+toSp[0]);
            }
        });

        toSpawn.forEach((m, t) ->
        {
            for(int i = 0; i < t; i++)
            {
                //Bukkit.getLogger().info("Spawning: "+m.getName()+" - "+t);
                m.spawnMonster(l1);
            }
        });
    }

    public Location getCheckpoint()
    {
        return checkpoint;
    }
}
