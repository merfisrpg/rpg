package sk.perri.rpg.area;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import sk.perri.rpg.Holder;
import sk.perri.rpg.RPG;
import sk.perri.rpg.classes.Hero;

public class AreaExecutor implements CommandExecutor
{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(!(sender instanceof Player))
        {
            sender.sendMessage("Players only!");
            return true;
        }

        if(args.length == 0)
        {
            showArea(sender);
            return true;
        }

        if(args[0].equalsIgnoreCase("chp") || args[0].equalsIgnoreCase("checkpoint"))
        {
            Hero h = Holder.getPlayer(sender.getName());

            if(h == null || h.getArea() == null)
            {
                sender.sendMessage(RPG.getMsg("noarea").get(0));
                return true;
            }

            if(h.getArea().getCheckpoint() == null)
            {
                sender.sendMessage(RPG.getMsg("nochparea").get(0));
                return true;
            }

            ((Player) sender).teleport(h.getArea().getCheckpoint());
            sender.sendMessage(RPG.getMsg("chparea").get(0));
            return true;
        }

        if(sender.isOp() && args[0].equalsIgnoreCase("spawn"))
        {
            if(args.length != 2)
            {
                sender.sendMessage("§c/area spawn <area>");
                return true;
            }

            Area a = Holder.areas.get(args[1]);

            if(a == null)
            {
                sender.sendMessage("§cZona nebola najdena!");
                return true;
            }

            a.spawnMonsters();
            sender.sendMessage("§aSpawnute!");
            return true;
        }

        showArea(sender);
        return true;
    }

    public void showArea(CommandSender sender)
    {
        Hero h = Holder.getPlayer(sender.getName());

        if(h == null || h.getArea() == null)
        {
            sender.sendMessage(RPG.getMsg("noarea").get(0));
            return;
        }

        RPG.getMsg("inarea").forEach(m ->
                sender.sendMessage(m.replace("{area}", h.getArea().getTitle())));
    }
}
