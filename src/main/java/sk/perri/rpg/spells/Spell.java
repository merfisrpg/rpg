package sk.perri.rpg.spells;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import sk.perri.rpg.inventories.ItemFactory;

import java.util.List;

public class Spell
{
    int id;
    private String name;
    private SpellAction action;
    private int cooldown;
    private int power;
    private ItemStack is;

    public Spell(int id, String name, int cooldown, int power, List<String> desc, String item)
    {
        this.id = id;
        this.name = ChatColor.translateAlternateColorCodes('&', name);
        this.cooldown = cooldown;
        this.power = power;

        is = ItemFactory.createItem(Material.matchMaterial(item), 1, -1, this.name, desc, false);
    }

    public void setAction(SpellAction action)
    {
        this.action = action;
    }

    public String getName()
    {
        return name;
    }

    public int getCooldown()
    {
        return cooldown;
    }

    public SpellAction getAction()
    {
        return action;
    }

    public int getId()
    {
        return id;
    }

    public int getPower() { return power; }

    public ItemStack getItem()
    {
        return is;
    }
}
