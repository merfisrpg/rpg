package sk.perri.rpg.spells;

import org.bukkit.entity.Player;

public interface SpellAction
{
    public abstract void cast(Player player);
}
