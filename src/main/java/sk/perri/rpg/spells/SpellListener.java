package sk.perri.rpg.spells;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import sk.perri.rpg.Holder;
import sk.perri.rpg.RPG;
import sk.perri.rpg.inventories.Inventories;

import java.util.ArrayList;
import java.util.List;

public class SpellListener implements Listener
{
    @EventHandler
    public void onSpell(PlayerInteractEvent event)
    {
        if(!isWand(event.getPlayer()))
            return;

        if(event.getAction().toString().toLowerCase().contains("left"))
        {
            List<String> lore = event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getLore();
            if(lore.size() < 4)
                return;

            String spell = ChatColor.stripColor(lore.get(lore.size()-1));

            Holder.spells.get(spell).getAction().cast(event.getPlayer());
            event.getPlayer().sendMessage(RPG.getMsg("castspell").get(0).replace("{spell}", Holder.spells.get(spell).getName()));
        }
        else if(event.getAction().toString().toLowerCase().contains("right"))
        {
            if(Inventories.plrInv.containsKey(event.getPlayer().getName()))
            {
                ItemStack iss = event.getPlayer().getInventory().getItemInMainHand();
                event.getPlayer().getInventory().clear();
                event.getPlayer().getInventory().setContents(Inventories.plrInv.get(event.getPlayer().getName()));
                Inventories.plrInv.remove(event.getPlayer().getName());
                event.getPlayer().getInventory().setItemInMainHand(iss);
            }
            else
            {
                Inventories.plrInv.put(event.getPlayer().getName(), event.getPlayer().getInventory().getContents());
                ItemStack iss = event.getPlayer().getInventory().getItemInMainHand();
                event.getPlayer().getInventory().clear();
                event.getPlayer().getInventory().setItemInMainHand(iss);
                Holder.spells.values().forEach(s -> event.getPlayer().getInventory().addItem(s.getItem()));
                Bukkit.getLogger().info(Holder.spells.values().size()+"");
            }
        }

        event.setCancelled(true);
    }

    private boolean isWand(Player player)
    {
        return !(player.getInventory().getItemInMainHand() == null ||
                !player.getInventory().getItemInMainHand().hasItemMeta() ||
                !player.getInventory().getItemInMainHand().getItemMeta().hasDisplayName() ||
                !player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().contains(
                        ChatColor.stripColor(Holder.items.get("palicka").getItemMeta().getDisplayName())));
    }

    private boolean isWand(ItemStack is)
    {
        return !(is == null ||
                !is.hasItemMeta() ||
                !is.getItemMeta().hasDisplayName() ||
                !is.getItemMeta().getDisplayName().contains(
                        ChatColor.stripColor(Holder.items.get("palicka").getItemMeta().getDisplayName())));
    }

    @EventHandler
    public void onDmg(EntityDamageByEntityEvent event)
    {
        if(event.getEntity() instanceof Player && event.getDamager().hasMetadata("spell"))
        {
            switch (event.getDamager().getMetadata("spell").get(0).asInt())
            {
                case 0: event.setDamage(event.getDamager().getMetadata("power").get(0).asInt()); break;
                case 2:
                    event.setDamage(0);
                    event.getEntity().setVelocity(event.getEntity().getLocation().getDirection().setY(0).normalize().multiply(-1));
                    event.setCancelled(true);
                    break;
            }
        }
    }

    @EventHandler
    public void onSwitch(PlayerItemHeldEvent event)
    {
        if(isWand(event.getPlayer().getInventory().getItem(event.getPreviousSlot())) &&
                Inventories.plrInv.containsKey(event.getPlayer().getName()) &&
                event.getPlayer().getInventory().getItem(event.getNewSlot()) != null &&
                event.getPlayer().getInventory().getItem(event.getNewSlot()).hasItemMeta() &&
                event.getPlayer().getInventory().getItem(event.getNewSlot()).getItemMeta().hasDisplayName())
        {
            ItemMeta im = event.getPlayer().getInventory().getItem(event.getPreviousSlot()).getItemMeta();
            List<String> nlore = new ArrayList<>();
            for(int i = 0; i < Holder.items.get("palicka").getItemMeta().getLore().size(); i++)
            {
                nlore.add(event.getPlayer().getInventory().getItem(event.getPreviousSlot()).getItemMeta().getLore().get(i));
            }

            nlore.add("");
            nlore.add("§7Kouzlo:");
            nlore.add("§9"+ChatColor.stripColor(event.getPlayer().getInventory().getItem(
                    event.getNewSlot()).getItemMeta().getDisplayName()));
            im.setLore(nlore);

            event.getPlayer().getInventory().getItem(event.getPreviousSlot()).setItemMeta(im);
            event.getPlayer().getInventory().setHeldItemSlot(event.getPreviousSlot());
        }
    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent event)
    {
        if(Inventories.plrInv.containsKey(event.getPlayer().getName()))
            event.setCancelled(true);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event)
    {
        if(Inventories.plrInv.containsKey(event.getPlayer().getName()))
        {
            event.getPlayer().getInventory().setContents(Inventories.plrInv.get(event.getPlayer().getName()));
            Inventories.plrInv.remove(event.getPlayer().getName());
        }
    }
}
