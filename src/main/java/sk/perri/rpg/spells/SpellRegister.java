package sk.perri.rpg.spells;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftFirework;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LlamaSpit;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.metadata.FixedMetadataValue;
import sk.perri.rpg.Holder;
import sk.perri.rpg.RPG;

public class SpellRegister
{
    private SpellRegister() {}

    public static void register()
    {
        Spell sp0 = Holder.spells.get(Holder.spellsN.get(0));
        sp0.setAction(player ->
        {
            LlamaSpit spit = player.launchProjectile(LlamaSpit.class);
            spit.setMetadata("spell", new FixedMetadataValue(RPG.instance, 0));
            spit.setMetadata("power", new FixedMetadataValue(RPG.instance, sp0.getPower()));
        });

        Spell sp1 = Holder.spells.get(Holder.spellsN.get(1));
        sp1.setAction(player ->
        {
            Firework fw = player.getWorld().spawn(player.getLocation(), Firework.class);
            FireworkMeta fwm = fw.getFireworkMeta();
            fwm.addEffect(FireworkEffect.builder().flicker(false).trail(true).withColor(Color.RED, Color.GREEN).
                    withFade(Color.LIME, Color.OLIVE).with(FireworkEffect.Type.BALL_LARGE).build());
            fwm.setPower(2);
            fw.setFireworkMeta(fwm);
        });

        Spell sp2 = Holder.spells.get(Holder.spellsN.get(2));
        sp2.setAction(player ->
        {
            LlamaSpit spit = player.launchProjectile(LlamaSpit.class);
            spit.setMetadata("spell", new FixedMetadataValue(RPG.instance, 2));
            spit.setMetadata("power", new FixedMetadataValue(RPG.instance, sp2.getPower()));
        });
    }
}
