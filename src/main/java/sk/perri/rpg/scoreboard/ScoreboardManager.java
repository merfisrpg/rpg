package sk.perri.rpg.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import sk.perri.rpg.Holder;
import sk.perri.rpg.Utils;
import sk.perri.rpg.classes.Hero;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScoreboardManager
{
    private Scoreboard board;
    private Objective lvls;
    private List<Objective> objs = new ArrayList<>();
    private HashMap<Integer, String> text = new HashMap<>();
    private Hero h;
    private Player p;
    private int currentBoard = 0;
    private int teamCounter = 0;

    public ScoreboardManager(Hero h, Player p)
    {
        this.h = h;
        this.p = p;

        board = Bukkit.getScoreboardManager().getNewScoreboard();
        lvls = board.registerNewObjective("lvls", "level");
        Holder.players.forEach((k, v) -> lvls.getScore(k).setScore(v.getLevel()));
        lvls.setDisplaySlot(DisplaySlot.PLAYER_LIST);

        try
        {
            for(Map.Entry<String, List<String>> en : Holder.boardsTemplates.entrySet())
            {
                Objective obj = board.registerNewObjective(en.getKey(), "dummy");
                obj.setDisplayName(Holder.boardTitle);

                int score = en.getValue().size();
                for(String s : en.getValue())
                {
                    Team t = board.registerNewTeam("t"+teamCounter);
                    t.addEntry(Utils.getChatColorHash(teamCounter));
                    t.setPrefix(s.length() > 3 ? s.substring(0, 3) : s);
                    obj.getScore(Utils.getChatColorHash(teamCounter)).setScore(score);

                    text.put(teamCounter, s);
                    teamCounter++;
                    score--;
                }

                objs.add(obj);
            }

            if(objs.get(currentBoard).getName().equalsIgnoreCase("quests") && h.getOpenQuests().size() == 0)
                currentBoard = (currentBoard+1)%objs.size();

            objs.get(currentBoard).setDisplaySlot(DisplaySlot.SIDEBAR);

            p.setScoreboard(board);

            updateBoard();
        }
        catch (Exception e)
        {
            Bukkit.getLogger().warning("Board creating error: "+e.toString());
        }
    }

    public void switchBoard()
    {
        objs.get(currentBoard).setDisplaySlot(null);
        currentBoard = (currentBoard+1)%objs.size();

        if(objs.get(currentBoard).getName().equalsIgnoreCase("quests") && h.getOpenQuests().size() == 0)
            currentBoard = (currentBoard+1)%objs.size();

        objs.get(currentBoard).setDisplaySlot(DisplaySlot.SIDEBAR);
    }

    public void updateBoard()
    {
        for(int i = 0; i < teamCounter; i++)
        {
            Team tt = board.getTeam("t"+i);
            String pl = text.get(i);
            String res = pl.
                    replace("{class}", Holder.clazzes.get(h.getClazz()).getName()).
                    replace("{level}", h.getLevel()+"").
                    replace("{crystals}", h.getCrystals()+"").
                    replace("{money}", h.getGolds()+"").
                    replace("{voted}", "0").
                    replace("{rank}", "-").
                    replace("{ping}", ((CraftPlayer) p).getHandle().ping+"").
                    replace("{pet}", "-").
                    replace("{lvlpet}", "-");


            if(res.length() >= 32)
                res = res.substring(0, 32);

            String p1 = res;
            String p2 = "";

            if(res.length() > 16)
            {
                int point = 16;
                if(res.charAt(15) == '§')
                    point = 15;
                else if(res.charAt(14) == '§')
                    point = 14;

                p1 = res.substring(0, point);
                String col = "§";

                for(int j = point; j > -1; j--)
                {
                    if(res.charAt(j) == '§')
                    {
                        col += res.charAt(j + 1);
                        break;
                    }
                }

                p2 = col+res.substring(point);
                p2 = p2.length() > 16 ? p2.substring(0, 16) : p2;
            }

            tt.setPrefix(p1);
            tt.setSuffix(p2);
        }
    }
}
