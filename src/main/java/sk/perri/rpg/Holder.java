package sk.perri.rpg;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import sk.perri.rpg.area.Area;
import sk.perri.rpg.classes.Clazz;
import sk.perri.rpg.classes.Hero;
import sk.perri.rpg.classes.Monster;
import sk.perri.rpg.classes.Quest;
import sk.perri.rpg.database.DBUtils;
import sk.perri.rpg.database.ResultRow;
import sk.perri.rpg.spells.Spell;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Holder
{
    private Holder() {}

    public static HashMap<String, Hero> players = new HashMap<>();

    public static Hero getPlayer(String player)
    {
        if(players.containsKey(player))
            return players.get(player);

        try
        {
            ResultSet rs = DBUtils.getFromDBRaw("SELECT COUNT(*) AS total FROM players WHERE nick='"+player+"'");
            if(rs != null && rs.next() && rs.getInt("total") >= 1)
                return new Hero(player);
        }
        catch (SQLException e)
        {
            Bukkit.getLogger().warning("Unable to get player "+player+" from DB: "+e.getMessage());
        }

        return null;
    }

    public static HashMap<String, Monster> monsters = new HashMap<>();
    public static HashMap<String, ItemStack> items = new HashMap<>();

    public static ItemStack getNewItem(Player pla, String iName)
    {
        if(!items.containsKey(iName))
            return null;

        ItemStack is = new ItemStack(items.get(iName));

        ItemMeta im = is.getItemMeta();
        im.setDisplayName(im.getDisplayName().replace("{player}", pla.getDisplayName()));
        List<String> trans = new ArrayList<>();
        im.getLore().forEach(l -> trans.add(l.replace("{player}", pla.getDisplayName())));
        im.setLore(trans);
        is.setItemMeta(im);

        return is;
    }

    public static String motd = "";

    public static HashMap<Integer, Quest> quests = new HashMap<>();

    public static List<Quest> getQuestsByOwner(String owner)
    {
        List<Quest> res = new ArrayList<>();

        quests.values().forEach(q -> { if(q.getOwner().equalsIgnoreCase(ChatColor.stripColor(owner))) res.add(q);});

        return res;
    }

    public static HashMap<String, String> prefixes = new HashMap<>();

    public static HashMap<String, Clazz> clazzes =  new HashMap<>();

    public static HashMap<String, Area> areas = new HashMap<>();

    public static LinkedHashMap<String, Spell> spells = new LinkedHashMap<>();
    public static Vector<String> spellsN = new Vector<>();

    public static HashMap<String, String> tpReq = new HashMap<>();
    public static HashMap<String, Integer> tpReqTimer = new HashMap<>();
    public static HashMap<String, List<String>> boardsTemplates = new HashMap<>();
    public static String boardTitle = "";
    public static int boardSwitchTime = 6000;
    public static HashMap<Integer, String> lvlColors = new HashMap<>();
}
