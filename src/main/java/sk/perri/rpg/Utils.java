package sk.perri.rpg;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Statistic;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class Utils
{
    private Utils() {}

    public static Location parseLocation(ConfigurationSection cs)
    {
        Location res;

        if(cs.isSet("pitch") && cs.isSet("yaw"))
            res = new Location(Bukkit.getWorld(cs.getString("world")), cs.getInt("x"), cs.getInt("y"),
                cs.getInt("z"), (float) cs.getDouble("yaw"), (float) cs.getDouble("pitch"));
        else
            res = new Location(Bukkit.getWorld(cs.getString("world")), cs.getInt("x"), cs.getInt("y"),
                    cs.getInt("z"));

        return res;
    }

    public static String getChatColorHash(int number)
    {
        String s = Integer.toHexString(number);
        StringBuilder res = new StringBuilder();

        for(Character c : s.toCharArray())
        {
            res.append("§").append(c);
        }

        return res.toString();
    }

    public static String getPlaceholder(String input)
    {
        String message = "";
        int open = input.indexOf("{");
        int close = input.indexOf("}");
        if(open != -1 && close != -1)
        {
            message = input.substring(open + 1, close);
        }
        return message;
    }

    public static String getPlayTime(Player player)
    {
        int playtime = player.getStatistic(Statistic.PLAY_ONE_TICK)/20;
        int sec = playtime % 60;
        int min = Math.floorDiv(playtime , 60)%60;
        int hours = Math.floorDiv(playtime , 60*60)%24;
        int days = Math.floorDiv(playtime , 60*60*24);

        return (days > 0 ? days+"d " : "")+(hours > 0 ? hours+"h " : "")+min+"m "+sec+"s";
    }
}
