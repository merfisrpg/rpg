package sk.perri.rpg.levels;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import sk.perri.rpg.Holder;
import sk.perri.rpg.RPG;
import sk.perri.rpg.classes.Hero;
import sk.perri.rpg.levels.Levels;

public class ExpListener implements Listener
{
    @EventHandler
    public void onExpCollect(PlayerExpChangeEvent event)
    {
        Hero h = Holder.players.get(event.getPlayer().getName());
        if(h != null)
        {
            h.setExp(h.getExp() + event.getAmount());
            RPG.getMsg("gainexp").forEach(m -> event.getPlayer().getPlayer().sendMessage(m.replace("{exp}", event.getAmount() + "")));
            float xp = h.getExp()/(float) Levels.getExpForLevel(h.getLevel());
            event.getPlayer().setExp(Math.min(xp, 1));
        }

        event.setAmount(0);
    }

    @EventHandler
    public void onLevelUp(PlayerLevelChangeEvent event)
    {
        Bukkit.getLogger().info("Old lvl: "+event.getOldLevel()+" New lvl: "+event.getNewLevel());

        Hero h = Holder.players.get(event.getPlayer().getName());
        if(h != null)
        {
            int lvl = event.getOldLevel();
            do
            {
                h.setLevel(lvl+1);
                h.setExp(h.getExp()-Levels.getExpForLevel(lvl));

                lvl++;
            }
            while(h.getExp() > Levels.getExpForLevel(lvl));

            event.getPlayer().setLevel(h.getLevel());
            event.getPlayer().setExp(Math.min(h.getExp()/(float) Levels.getExpForLevel(h.getLevel()), 1.0f));
            RPG.getMsg("levelup").forEach(m -> event.getPlayer().sendMessage(m.replace("{level}", h.getLevel()+"")));

        }
    }

    @EventHandler
    public void onDeath(PlayerDeathEvent event)
    {
        event.setKeepInventory(true);
        event.setKeepLevel(true);
    }
}
