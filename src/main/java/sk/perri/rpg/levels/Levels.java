package sk.perri.rpg.levels;

import org.apache.commons.lang.StringUtils;
import sk.perri.rpg.classes.Hero;

public class Levels
{
    // y=50*(1.5^(x-1))

    public static long getExpForLevel(int level)
    {
        return Math.round(50*(Math.pow(1.5, level-1)));
    }

    public static String showExpProgress(long exp, int currentLevel)
    {
        String ch = "▒";
        int len = 12;
        int col = Math.round(len*(exp/(float) getExpForLevel(currentLevel)));
        return "§a"+StringUtils.repeat(ch, col)+"§7"+StringUtils.repeat(ch,len-col);
    }

    public static void addExp(Hero h, long exp)
    {
        int j = h.getLevel();
        while(exp > getExpForLevel(j))
        {
            exp -= getExpForLevel(j);
            j++;
            h.setExp(exp);
            h.setLevel(j);
        }
    }
}
