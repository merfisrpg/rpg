package sk.perri.rpg;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.citizensnpcs.api.CitizensAPI;
import net.minecraft.server.v1_12_R1.PacketDataSerializer;
import net.minecraft.server.v1_12_R1.PacketPlayOutCustomPayload;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandExecutor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.java.JavaPlugin;
import sk.perri.rpg.area.Area;
import sk.perri.rpg.area.AreaExecutor;
import sk.perri.rpg.classes.Clazz;
import sk.perri.rpg.classes.Monster;
import sk.perri.rpg.classes.Quest;
import sk.perri.rpg.commands.EssentialExecutor;
import sk.perri.rpg.commands.LevelExecutor;
import sk.perri.rpg.commands.MonsterExecutor;
import sk.perri.rpg.commands.RPGExecutor;
import sk.perri.rpg.database.DBUtils;
import sk.perri.rpg.inventories.InventoryListener;
import sk.perri.rpg.inventories.ItemFactory;
import sk.perri.rpg.levels.ExpListener;
import sk.perri.rpg.listeners.*;
import sk.perri.rpg.spells.Spell;
import sk.perri.rpg.spells.SpellListener;
import sk.perri.rpg.spells.SpellRegister;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RPG extends JavaPlugin
{
    public static RPG instance;
    private static YamlConfiguration msgs, mons, items, quests, chat, clazzes, areas, spells, boardsT;

    @Override
    public void onEnable()
    {
        instance = this;

        getDataFolder().mkdir();
        if(!new File(getDataFolder().getPath(), "config.yml").exists())
        {
            getConfig().options().copyDefaults(true);
            saveConfig();
        }

        loadConfig();

        DBUtils.connect(getConfig().getString("db_host"), getConfig().getString("db_user"),
                getConfig().getString("db_pass"), getConfig().getString("db_name"),
                getConfig().getString("db_port"));

        getServer().getPluginManager().registerEvents(new OnJoinQuit(), this);
        getServer().getPluginManager().registerEvents(new InventoryListener(), this);
        getServer().getPluginManager().registerEvents(new ExpListener(), this);
        getServer().getPluginManager().registerEvents(new MonsterListener(), this);
        getServer().getPluginManager().registerEvents(new QuestListener(), this);
        getServer().getPluginManager().registerEvents(new ChatListener(), this);
        getServer().getPluginManager().registerEvents(new SpellListener(), this);
        CitizensAPI.registerEvents(new NPCQuestListener());

        LevelExecutor le = new LevelExecutor();
        getCommand("level").setExecutor(le);
        getCommand("addexp").setExecutor(le);
        getCommand("monster").setExecutor(new MonsterExecutor());
        getCommand("rpg").setExecutor(new RPGExecutor());
        getCommand("area").setExecutor(new AreaExecutor());

        CommandExecutor ess = new EssentialExecutor();
        getCommand("pm").setExecutor(ess);
        getCommand("reply").setExecutor(ess);
        getCommand("tp").setExecutor(ess);
        getCommand("tphere").setExecutor(ess);
        getCommand("money").setExecutor(ess);
        getCommand("crystals").setExecutor(ess);

        new Hearth();
        getLogger().info("Aktivoval som sa!");
    }

    @Override
    public void onDisable()
    {
        getLogger().info("Deaktivoval som sa!");
    }

    public static List<String> getMsg(String key)
    {
        List<String> raw = new ArrayList<>();
        List<String> res = new ArrayList<>();

        if(!msgs.isSet(key))
            raw.add("UNDEFINED");
        else
        {
            if(msgs.isList(key))
                raw.addAll(msgs.getStringList(key));
            else
                raw.add(msgs.getString(key));
        }

        raw.forEach(m -> res.add(ChatColor.translateAlternateColorCodes('&', m)));
        return res;
    }

    private void loadItems()
    {
        if(items == null)
            return;

        try
        {
            items.getKeys(false).forEach(k ->
                Holder.items.put(k, ItemFactory.createItem(items.getConfigurationSection(k))));
        }
        catch (Exception e)
        {
            getLogger().warning("Unable to create items from items.yml: "+e.toString());
        }
    }

    private void loadQuests()
    {
        if(quests == null)
            return;

        try
        {
            quests.getKeys(false).forEach(k ->
            {
                Quest q = new Quest(Integer.parseInt(k), quests.getConfigurationSection(k));
                Holder.quests.put(Integer.parseInt(k), q);
            });
        }
        catch (Exception e)
        {
            getLogger().warning("Unable to create quest from quests.yml: "+e.toString());
        }
    }

    private void loadClazzes()
    {
        if(clazzes == null)
            return;

        try
        {
            clazzes.getKeys(false).forEach(k -> Holder.clazzes.put(k, new Clazz(k, clazzes.getConfigurationSection(k))));
        }
        catch (Exception e)
        {
            getLogger().warning("Unable to create class from classes.yml: "+e.toString());
        }
    }

    private void loadScoreboards()
    {
        if(boardsT == null)
            return;

        try
        {
            boardsT.getKeys(false).forEach(k ->
            {
                if(k.equalsIgnoreCase("title"))
                {
                    Holder.boardTitle = ChatColor.translateAlternateColorCodes('&', boardsT.getString(k));
                    return;
                }
                else if(k.equalsIgnoreCase("switchtime"))
                {
                    Holder.boardSwitchTime = boardsT.getInt(k);
                    return;
                }

                List<String> res = new ArrayList<>();
                boardsT.getStringList(k).forEach(l -> res.add(ChatColor.translateAlternateColorCodes('&', l)));
                Holder.boardsTemplates.put(k, res);
            });
        }
        catch (Exception e)
        {
            getLogger().warning("Unable to create boards from scoreboard.yml: "+e.toString());
        }
    }

    private void loadAreas()
    {
        if(areas == null)
            return;

        try
        {
            areas.getKeys(false).forEach(k -> Holder.areas.put(k, new Area(k, areas.getConfigurationSection(k))));
        }
        catch (Exception e)
        {
            getLogger().warning("Unable to create quest from areas.yml: "+e.toString());
        }
    }

    private void makeBooks()
    {
        ItemStack story = new ItemStack(Material.WRITTEN_BOOK, 1);
        BookMeta bm = (BookMeta) story.getItemMeta();
        getMsg("bookstory").forEach(l -> bm.addPage(ChatColor.translateAlternateColorCodes('&', l)));
        bm.setAuthor("Perri & feinnn");
        bm.setTitle("Pribeh");
        story.setItemMeta(bm);
        Holder.items.put("pribeh", story);

        ItemStack mech = new ItemStack(Material.WRITTEN_BOOK, 1);
        BookMeta bmm = (BookMeta) mech.getItemMeta();
        getMsg("bookgame").forEach(l -> bmm.addPage(ChatColor.translateAlternateColorCodes('&', l)));
        bmm.setAuthor("Perri & feinnn");
        bmm.setTitle("Mechanika");
        mech.setItemMeta(bmm);
        Holder.items.put("mechanika", mech);
    }

    public void openStoryBook(Player p)
    {
        ItemStack is = p.getInventory().getItemInMainHand();

        p.getInventory().setItemInMainHand(Holder.items.get("pribeh").clone());

        if(Holder.players.containsKey(p.getName()))
            Holder.players.get(p.getName()).setTut(1);
        openBookReq(p, is);
    }

    public void openGameBook(Player p)
    {
        ItemStack is = p.getInventory().getItemInMainHand();

        p.getInventory().setItemInMainHand(Holder.items.get("mechanika").clone());

        if(Holder.players.containsKey(p.getName()))
            Holder.players.get(p.getName()).setTut(-1);

        openBookReq(p, is);
    }

    public void openBookReq(Player p, ItemStack is)
    {
        getServer().getScheduler().runTaskLater(this, () ->
        {
            ByteBuf buf = Unpooled.buffer(256);
            buf.setByte(0, (byte)0);
            buf.writerIndex(1);

            PacketPlayOutCustomPayload packet = new PacketPlayOutCustomPayload("MC|BOpen", new PacketDataSerializer(buf));
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
            p.getInventory().setItemInMainHand(is);
        }, 1);
    }

    public void loadChatConfig()
    {
        try
        {
            File chatf = new File(getDataFolder(), "chat.yml");
            chat.load(chatf);

            Holder.prefixes.put("level", ChatColor.translateAlternateColorCodes('&', chat.getString("level")));
            Holder.prefixes.put("delimeter", ChatColor.translateAlternateColorCodes('&', chat.getString("delimeter")));
            Holder.prefixes.put("pm", ChatColor.translateAlternateColorCodes('&', chat.getString("pm")));

            chat.getConfigurationSection("groups").getKeys(false).forEach(k ->
            {
                Holder.prefixes.put(k+"p", ChatColor.translateAlternateColorCodes('&', chat.getString("groups."+k+".prefix")));
                Holder.prefixes.put(k+"s", ChatColor.translateAlternateColorCodes('&', chat.getString("groups."+k+".suffix")));
                Holder.prefixes.put(k+"m", ChatColor.translateAlternateColorCodes('&', chat.getString("groups."+k+".msg")));
                Holder.prefixes.put(k+"lh", ChatColor.translateAlternateColorCodes('&', StringUtils.join(chat.getStringList("groups."+k+".levelhover"), '\n')));
                Holder.prefixes.put(k+"ph", ChatColor.translateAlternateColorCodes('&', StringUtils.join(chat.getStringList("groups."+k+".playerhover"), '\n')));
            });

            chat.getConfigurationSection("lvlcolor").getKeys(false).forEach(k ->
            {
               Holder.lvlColors.put(Integer.parseInt(k), ChatColor.translateAlternateColorCodes('&',
                       chat.getString("lvlcolor."+k)));
            });
        }
        catch (Exception e)
        {
            getLogger().warning("Unable to load chat.yml: "+e.toString());
        }
    }

    public void loadSpells()
    {
        try
        {
            spells.getKeys(false).forEach(k ->
            {
                List<String> resd = new ArrayList<>();
                spells.getStringList(k+".desc").forEach(l ->
                        resd.add(ChatColor.translateAlternateColorCodes('&', l.replace("{power}",
                                spells.getString(k+".power")))));

                Spell sp = new Spell(Integer.parseInt(k), spells.getString(k+".name"), spells.getInt(k+".cooldown"),
                        spells.getInt(k+".power"), resd, spells.getString(k+".item"));
                Holder.spells.put(ChatColor.stripColor(sp.getName()), sp);
                Holder.spellsN.add(ChatColor.stripColor(sp.getName()));
            });
        }
        catch (Exception e)
        {
            getLogger().warning("Unable to load spells.yml: "+e.toString());
        }
    }

    public void loadConfig()
    {
        msgs = new YamlConfiguration();
        mons = new YamlConfiguration();
        items = new YamlConfiguration();
        quests = new YamlConfiguration();
        chat = new YamlConfiguration();
        clazzes = new YamlConfiguration();
        areas = new YamlConfiguration();
        spells = new YamlConfiguration();
        boardsT = new YamlConfiguration();

        if(!new File(getDataFolder().getPath(), "messages.yml").exists())
            saveResource("messages.yml", true);
        if(!new File(getDataFolder().getPath(), "monsters.yml").exists())
            saveResource("monsters.yml", true);
        if(!new File(getDataFolder().getPath(), "items.yml").exists())
            saveResource("items.yml", true);
        if(!new File(getDataFolder().getPath(), "chat.yml").exists())
            saveResource("chat.yml", true);
        if(!new File(getDataFolder().getPath(), "classes.yml").exists())
            saveResource("classes.yml", true);
        if(!new File(getDataFolder().getPath(), "quests.yml").exists())
            saveResource("quests.yml", true);
        if(!new File(getDataFolder().getPath(), "areas.yml").exists())
            saveResource("areas.yml", true);
        if(!new File(getDataFolder().getPath(), "spells.yml").exists())
            saveResource("spells.yml", true);
        if(!new File(getDataFolder().getPath(), "scoreboard.yml").exists())
            saveResource("scoreboard.yml", true);

        File msgf = new File(getDataFolder(), "messages.yml");
        File monf = new File(getDataFolder(), "monsters.yml");
        File itef = new File(getDataFolder(), "items.yml");
        File quef = new File(getDataFolder(), "quests.yml");
        File classf = new File(getDataFolder(), "classes.yml");
        File areaf = new File(getDataFolder(), "areas.yml");
        File spef = new File(getDataFolder(), "spells.yml");
        File scof = new File(getDataFolder(), "scoreboard.yml");

        try
        {
            msgs.load(msgf);
            mons.load(monf);
            items.load(itef);
            quests.load(quef);
            clazzes.load(classf);
            areas.load(areaf);
            spells.load(spef);
            boardsT.load(scof);

            mons.getKeys(false).forEach(k -> Holder.monsters.put(ChatColor.stripColor(k).toLowerCase(),
                    new Monster(ChatColor.translateAlternateColorCodes('&', k), mons.getConfigurationSection(k))));

            StringBuilder sb = new StringBuilder();
            RPG.getMsg("motd").forEach(s -> sb.append(s).append("\n"));
            Holder.motd = sb.toString();

            loadItems();
            loadQuests();
            loadChatConfig();
            makeBooks();
            loadClazzes();
            loadAreas();
            loadScoreboards();

            loadSpells();
            SpellRegister.register();

            getLogger().info("RPG Configs (re)loaded!");
        }
        catch (IOException e)
        {
            getLogger().warning("Unable to read configs: "+e.toString());
        }
        catch (InvalidConfigurationException e)
        {
            getLogger().warning("Invalid configs: "+e.toString());
        }
    }
}
