package sk.perri.rpg.database;

import org.bukkit.Bukkit;
import sk.perri.rpg.RPG;

import java.sql.*;
import java.util.Vector;

public class DBUtils
{
    private DBUtils() {}

    private static Connection conn = null;

    public static boolean connect(String host, String user, String pass, String dbname, String port)
    {
        try
        {
            conn = DriverManager.getConnection("jdbc:mysql://"+host+":"+port+"/"+dbname, user, pass);

            Bukkit.getScheduler().runTaskTimer(RPG.instance, () ->
            {
                try
                {
                    Statement stm = conn.createStatement();
                    ResultSet rs = stm.executeQuery("SELECT * FROM players LIMIT 1");
                    if(rs != null)
                        rs.next();

                    stm.close();
                }
                catch (SQLException e)
                {
                    Bukkit.getLogger().warning("Unable to refresh SQL connection: "+e.getMessage());
                }
            }, 72000, 72000);

            Bukkit.getLogger().info("Connected to the DB "+dbname);
            return true;
        }
        catch (SQLException e)
        {
            Bukkit.getLogger().warning("Unable to create DB connection: "+e.getMessage());
            return false;
        }
    }

    public static ResultSet getFromDBRaw(String sql)
    {
        try
        {
            Statement stm = conn.createStatement();
            return stm.executeQuery(sql);
        }
        catch (SQLException e)
        {
            Bukkit.getLogger().warning("Unable to get from the DB: "+e.getMessage());
            return null;
        }
    }

    public static Vector<ResultRow> getFromDB(String sql)
    {
        Vector<ResultRow> result = new Vector<>();

        try
        {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(sql);
            if(rs != null)
            {
                while(rs.next())
                {
                    ResultRow rr = new ResultRow();

                    for(int i = 0; i < rs.getMetaData().getColumnCount(); i++)
                    {
                        rr.addValue(rs.getMetaData().getColumnLabel(i+1), rs.getObject(i+1));
                    }

                    result.add(rr);
                }
            }

            stm.close();
        }
        catch (SQLException e)
        {
            Bukkit.getLogger().warning("Unable to get from DB: "+e.getMessage());
        }
        return result;
    }

    public static boolean saveToDB(String sql, Object... args)
    {
        try
        {
            PreparedStatement ps = conn.prepareStatement(sql);
            if(args != null && args.length > 0)
            {
                for(int i = 0; i < args.length; i++)
                {
                    ps.setObject(i+1, args[i]);
                }
            }
            boolean res = ps.execute();
            ps.close();
            return res;
        }
        catch (SQLException e)
        {
            Bukkit.getLogger().warning("Unable to save to the DB: "+e.getMessage());
            return false;
        }
    }
}
