package sk.perri.rpg.database;

import java.util.*;

public class ResultRow
{
    private Map<String, Object> row = new HashMap<>();
    private Vector<String> keys = new Vector<>();

    public void addValue(String key, Object value)
    {
        row.put(key, value);
        keys.add(key);
    }

    public Object getValue(String key)
    {
        return row.get(key);
    }

    public Collection<Object> getValues()
    {
        return row.values();
    }

    public Vector<String> getKeys()
    {
        return keys;
    }

    public void changeValue(String key, Object newValue)
    {
        if(row.containsKey(key))
            row.replace(key, newValue);
        else
            row.put(key, newValue);
    }

    public Map<String, Object> getRow()
    {
        return row;
    }
}
