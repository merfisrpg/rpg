package sk.perri.rpg.classes;

import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import sk.perri.rpg.Holder;
import sk.perri.rpg.RPG;
import sk.perri.rpg.area.Area;
import sk.perri.rpg.database.DBUtils;
import sk.perri.rpg.database.ResultRow;
import sk.perri.rpg.scoreboard.ScoreboardManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hero
{
    private String nick = "";
    private Player player;
    private long golds = 0;
    private int crystals = 0;
    private String demon = "";
    private int level = 1;
    private long exp = 0;
    private int tut = 2;
    private String group = "default";
    private String clazz = "";
    private Area area = null;
    private ScoreboardManager sm;

    private List<Integer> qcompleted = new ArrayList<>();
    private Map<Integer, Integer> qopened = new HashMap<>();

    public Hero(Player player)
    {
        if(player == null)
        {
            Bukkit.getLogger().warning("New HERO null player!");
            return;
        }

        nick = player.getName();
        this.player = player;

        player.getEffectivePermissions().forEach(p ->
        {
            if(p.getValue() && p.getPermission().contains("rpgchat.group"))
            {
                group = p.getPermission().split("\\.")[2];
            }
        });

        List<ResultRow> res = DBUtils.getFromDB("SELECT * FROM players WHERE nick='"+nick+"'");
        if(res.size() == 0)
        {
            DBUtils.saveToDB("INSERT INTO players VALUES(?, '', '', DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)", player.getName());
            tut = 0;
        }
        else
        {
            ResultRow rr = res.get(0);
            golds = (long) rr.getValue("gold");
            crystals = (int) rr.getValue("crystals");
            exp = (long) rr.getValue("exp");
            level = (int) rr.getValue("lvl");
            demon = (String) rr.getValue("demon");
            clazz = (String) rr.getValue("class");
        }

        res = DBUtils.getFromDB("SELECT * FROM quests WHERE nick='"+nick+"'");

        if(res.size() > 0)
        {
            res.forEach(rw ->
            {
                if((int) rw.getValue("status") == 1)
                    qopened.put((int) rw.getValue("qid"), (int) rw.getValue("status"));
                else
                    qcompleted.add((int) rw.getValue("qid"));
            });
        }

        sm = new ScoreboardManager(this, player);
    }

    public Hero(String nick)
    {
        this.nick = nick;
        this.player = null;

        List<ResultRow> res = DBUtils.getFromDB("SELECT * FROM players WHERE nick='"+nick+"'");

        if(res.size() != 0)
        {
            ResultRow rr = res.get(0);
            golds = (long) rr.getValue("gold");
            crystals = (int) rr.getValue("crystals");
            exp = (long) rr.getValue("exp");
            level = (int) rr.getValue("lvl");
            demon = (String) rr.getValue("demon");
            clazz = (String) rr.getValue("class");
        }
    }

    public void saveToDB()
    {
        DBUtils.saveToDB("UPDATE players SET demon=?, class=?, lvl=?, exp=?, gold=?, crystals=? WHERE nick = ?",
                demon, clazz, level, exp, golds, crystals, nick);
    }

    public String getDemon()
    {
        return demon;
    }

    public void setDemon(String demon)
    {
        this.demon = demon;
    }

    public int getLevel()
    {
        return level;
    }

    public long getExp()
    {
        return exp;
    }

    public void setLevel(int newLevel) { level = newLevel; }

    public void setExp(long newExp) { exp = newExp; }

    public void setTut(int newTut) { this.tut = newTut; }

    public int getTut() {
        return tut;
    }

    public int getQuestFinalStatus(int qid)
    {
        return qcompleted.contains(qid) ? -2 : qopened.containsKey(qid) ? 0 : -1;
    }

    public int getQuestStatus(int qid)
    {
        return qopened.getOrDefault(qid, -1);
    }

    public void openQuest(int qid)
    {
        if(!qopened.containsKey(qid) && !qcompleted.contains(qid))
        {
            qopened.put(qid, 0);

            DBUtils.saveToDB("INSERT INTO quests SET nick=?, qid=?, status=?", nick, qid, 0);
        }
    }

    public void comleteQuest(int qid)
    {
        if(!qcompleted.contains(qid))
        {
            if(qopened.containsKey(qid))
                qopened.remove(qid);
            qcompleted.add(qid);
            DBUtils.saveToDB("INSERT INTO quests SET nick=?, qid=?, status=? ON DUPLICATE KEY UPDATE status=?", nick, qid, -2, -2);

            Quest q = Holder.quests.get(qid);
            RPG.getMsg("questcompleted").forEach(m -> player.sendMessage(m.replace("{quest}", q.getName())));
            ((ExperienceOrb) player.getWorld().spawnEntity(player.getLocation(), EntityType.EXPERIENCE_ORB)).setExperience((int) q.getExp());
            addGold(q.getGold());
            addCrystal(q.getCrystal());
        }
    }

    public Map<Integer, Integer> getOpenQuests()
    {
        return qopened;
    }

    public void setQuestStatus(int qid, int newStatus)
    {
        qopened.put(qid, newStatus);
    }

    public void addGold(int add)
    {
        golds += add;
        RPG.getMsg("gaingold").forEach(m -> player.sendMessage(m.replace("{golds}", add+"")));
    }

    public void addCrystal(int add)
    {
        golds += add;
        RPG.getMsg("gaincrystal").forEach(m -> player.sendMessage(m.replace("{crystals}", add+"")));
    }

    public String getGroup()
    {
        return group;
    }
    public void setGroup(String group) { this.group = group; }

    public void setClazz(String newClazz)
    {
        this.clazz = newClazz;
    }
    public String getClazz()
    {
        return clazz;
    }

    public void setArea(Area area) { this.area = area; }
    public Area getArea() { return area; }

    public Player getPlayer()
    {
        return player;
    }

    public ScoreboardManager getSbManager()
    {
        return sm;
    }

    public long getGolds()
    {
        return golds;
    }

    public int getCrystals()
    {
        return crystals;
    }
}
