package sk.perri.rpg.classes;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import sk.perri.rpg.inventories.ItemFactory;

import java.util.ArrayList;
import java.util.List;

public class Clazz
{
    private String id;
    private String name;
    private List<String> lore = new ArrayList<>();
    private ItemStack item;

    public Clazz(String id, ConfigurationSection cs)
    {
        this.id = id;
        this.name = ChatColor.translateAlternateColorCodes('&', cs.getString("name"));
        cs.getStringList("lore").forEach(l -> lore.add(ChatColor.translateAlternateColorCodes('&', l)));

        item = ItemFactory.createItem(Material.CONCRETE, 1, cs.getInt("itemcolour"), this.name, lore, false);
    }

    public ItemStack getItem()
    {
        return item;
    }

    public String getName()
    {
        return name;
    }
}
