package sk.perri.rpg.classes;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import sk.perri.rpg.inventories.ItemFactory;

import java.util.ArrayList;
import java.util.List;

public class Quest
{
    private String name = "";
    private String type = "";
    private String target = "";
    private String owner = "";
    private int id = -1;
    private int amount = -1;
    private long exp = 0;
    private int gold = 0;
    private int crystal = 0;
    private List<String> items = new ArrayList<>();
    private List<String> shortdesc = new ArrayList<>();
    private List<String> longdesc = new ArrayList<>();
    private List<String> end = new ArrayList<>();

    public Quest(int id, ConfigurationSection cs)
    {
        name = cs.getString("name");
        this.id = id;
        type = cs.getString("type");
        target = cs.getString("target");
        owner = cs.getString("owner");
        String reward[] = cs.getString("reward").split(",");
        for (String s : reward)
        {
            String part[] = s.split(":");

            switch(part[0].toLowerCase())
            {
                case "ci": items.add(part[1]); break;
                case "exp": exp = Integer.parseInt(part[1]); break;
                case "gold": gold = Integer.parseInt(part[1]); break;
                case "crystal": crystal = Integer.parseInt(part[1]); break;
            }
        }
        amount = cs.getInt("amount");
        exp = cs.getLong("exp");
        gold = cs.getInt("gold");
        crystal = cs.getInt("crystal");
        cs.getStringList("shortdesc").forEach(l -> shortdesc.add(ChatColor.translateAlternateColorCodes('&', l)));
        cs.getStringList("longdesc").forEach(l -> longdesc.add(ChatColor.translateAlternateColorCodes('&', l)));
        cs.getStringList("end").forEach(l -> end.add(ChatColor.translateAlternateColorCodes('&', l)));
    }

    public String getOwner() { return owner; }
    public String getTarget() { return target; }

    public long getExp()
    {
        return exp;
    }

    public int getCrystal()
    {
        return crystal;
    }

    public int getGold()
    {
        return gold;
    }

    public String getType()
    {
        return type;
    }

    public int getAmount()
    {
        return amount;
    }

    public List<String> getEnd()
    {
        return end;
    }

    public List<String> getLongdesc()
    {
        return longdesc;
    }

    public List<String> getShortdesc()
    {
        return shortdesc;
    }

    public List<String> getItems() { return items; }

    public ItemStack getQuestItem(int status)
    {
        int data;
        String name = this.name;

        switch (status)
        {
            // open
            case -2: data = 4; name = "§e"+name; break;
            // done
            case -1: data = 8; name = "§7"+name; break;
            // available
            default: data = 5; name = "§a§l"+name;
        }

        return ItemFactory.createItem(Material.CONCRETE, 1, data, name, shortdesc, false);
    }

    public String getName()
    {
        return name;
    }

    public int getId()
    {
        return id;
    }
}
