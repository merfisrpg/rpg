package sk.perri.rpg.classes;

import org.bukkit.Location;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Creature;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;

public class Monster
{
    private String name = "";
    private int health = 5;
    private int dmg = 5;
    private int exps = 15;
    private EntityType type;
    private int behave = 0;

    public Monster(String name, ConfigurationSection cs)
    {
        this.name = name;
        health = cs.getInt("health");
        dmg = cs.getInt("damage");
        exps = cs.getInt("exp");
        behave = cs.getInt("behave");
        type = EntityType.valueOf(cs.getString("type"));
    }

    public Creature spawnMonster(Location loc)
    {
        Creature e = (Creature) loc.getWorld().spawnEntity(loc, type);

        e.setCustomNameVisible(true);
        e.setCustomName((behave < 0 ? "§c" : behave == 0 ? "§e" : "§a")+name);
        e.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
        e.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(dmg);
        e.setAI(behave < 0);

        return e;
    }

    public String getName()
    {
        return name;
    }

    public int getExps()
    {
        return exps;
    }
}
