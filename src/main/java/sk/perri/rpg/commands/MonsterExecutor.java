package sk.perri.rpg.commands;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import sk.perri.rpg.Holder;
import sk.perri.rpg.classes.Monster;

public class MonsterExecutor implements CommandExecutor
{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(!(sender instanceof Player))
        {
            sender.sendMessage("Iba hraci mozu pouzit tento prikaz");
            return true;
        }

        if(args.length == 0)
        {
            sender.sendMessage("§cNeviem najst tuto priseru");
            return true;
        }

        if(args[0].equalsIgnoreCase("list"))
        {
            Holder.monsters.keySet().forEach(k -> sender.sendMessage("§e"+k));
            return true;
        }

        if(Holder.monsters.containsKey(args[0].toLowerCase()))
        {
            Monster m = Holder.monsters.get(args[0].toLowerCase());
            m.spawnMonster(((Player) sender).getLocation());
            sender.sendMessage("§aPrisera "+m.getName()+" §aspawnuta!");
        }
        else
            sender.sendMessage("§cNeviem najst priseru: "+args[0].toLowerCase());

        return true;
    }
}
