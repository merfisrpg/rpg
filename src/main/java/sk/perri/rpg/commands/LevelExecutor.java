package sk.perri.rpg.commands;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerExpChangeEvent;
import sk.perri.rpg.Holder;
import sk.perri.rpg.RPG;
import sk.perri.rpg.classes.Hero;
import sk.perri.rpg.levels.Levels;

public class LevelExecutor implements CommandExecutor
{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(cmd.getName().equalsIgnoreCase("level"))
        {
            if(args.length == 1 && (!(sender instanceof Player) || sender.hasPermission("rpg.level.other") || sender.isOp()))
            {
                Hero h = Holder.getPlayer(args[0]);

                if(h == null)
                    sender.sendMessage(RPG.getMsg("notfound").get(0));
                else
                {
                    RPG.getMsg("levelother").forEach(m -> sender.sendMessage(m.replace("{player}", args[0])
                            .replace("{level}", h.getLevel()+"").replace("{exp}", h.getExp()+"")
                            .replace("{tonextlevel}", Levels.getExpForLevel(h.getLevel())+"")
                            .replace("{expperc}", Double.toString(Math.round(1000*h.getExp()/
                                    (double) Levels.getExpForLevel(h.getLevel()))/10.0)).replace("{expshow}",
                                    Levels.showExpProgress(h.getExp(), h.getLevel()))));
                }
            }
            else
            {
                Hero h = Holder.getPlayer(sender.getName());

                if(h == null)
                    sender.sendMessage(RPG.getMsg("notfound").get(0));
                else
                {
                    RPG.getMsg("level").forEach(m -> sender.sendMessage(m.replace("{level}",
                            h.getLevel() + "").replace("{exp}", h.getExp() + "")
                            .replace("{tonextlevel}", Levels.getExpForLevel(h.getLevel()) + "")
                            .replace("{expperc}", Double.toString(Math.round(1000 * h.getExp() /
                                    (double) Levels.getExpForLevel(h.getLevel()))/10.0)).replace("{expshow}",
                                    Levels.showExpProgress(h.getExp(), h.getLevel()))));
                }
            }
        }

        if(cmd.getName().equalsIgnoreCase("addexp"))
        {
            if(!sender.isOp() && !sender.hasPermission("rpg.addexp"))
            {
                sender.sendMessage(RPG.getMsg("noperms").get(0));
                return true;
            }

            if(args.length == 2 && StringUtils.isNumeric(args[1]))
            {
                if(Holder.players.containsKey(args[0]))
                {
                    ((ExperienceOrb) Bukkit.getPlayer(args[0]).getWorld().spawnEntity(
                            Bukkit.getPlayer(args[0]).getLocation(), EntityType.EXPERIENCE_ORB)).setExperience(Integer.parseInt(args[1]));
                    //Bukkit.getPluginManager().callEvent(new PlayerExpChangeEvent(Bukkit.getPlayer(args[0]), Integer.parseInt(args[1])));
                    sender.sendMessage("§aExpy pripisane");
                }
                else
                {
                    sender.sendMessage("§ePozeram do DB");
                    Hero h = Holder.getPlayer(args[0]);

                    if(h == null)
                    {
                        sender.sendMessage("§cHraca som nenasiel :/");
                        return true;
                    }

                    Levels.addExp(h, Integer.parseInt(args[1]));
                    h.saveToDB();
                    sender.sendMessage("§aExpy pripisane");
                }
            }
        }

        return true;
    }
}
