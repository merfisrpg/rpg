package sk.perri.rpg.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import sk.perri.rpg.Holder;
import sk.perri.rpg.RPG;
import sk.perri.rpg.classes.Hero;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class EssentialExecutor implements CommandExecutor
{
    private HashMap<Player, Player> comm = new HashMap<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(!(sender instanceof Player))
        {
            sender.sendMessage("Iba hraci mozu pouzit PM!");
            return true;
        }

        if(cmd.getName().equalsIgnoreCase("pm"))
        {
            if(args.length < 2)
            {
                sender.sendMessage("§c/pm <hrac> <sprava>");
                return true;
            }

            sendPM((Player) sender, args);
        }

        if(cmd.getName().equalsIgnoreCase("reply"))
        {
            if(args.length < 1)
            {
                sender.sendMessage("§c/r <sprava>");
                return true;
            }

            if(!comm.containsKey(sender))
            {
                sender.sendMessage("§cNemas komu odpisat!");
                return true;
            }

            sendPM((Player) sender, comm.get(sender), args);
        }

        if(cmd.getName().equalsIgnoreCase("tp"))
        {
            if(args.length < 1)
            {
                sender.sendMessage("§c/tp <hrac>");
                return true;
            }

            if(args.length == 1 && sender.hasPermission("rpg.tp"))
            {
                teleportPlayer((Player) sender, args[0]);
            }

            if(args.length == 2 && sender.hasPermission("rpg.tp.other"))
            {
                Player p1 = Bukkit.getPlayer(args[0]);
                if(p1 == null)
                    sender.sendMessage(RPG.getMsg("notfound").get(0));
                else
                    teleportPlayer(p1, args[0]);
            }
        }

        if(cmd.getName().equalsIgnoreCase("tphere"))
        {
            if (args.length < 1)
            {
                sender.sendMessage("§c/tphere <hrac>");
                return true;
            }

            Player p = Bukkit.getPlayer(args[0]);
            if(p == null)
            {
                sender.sendMessage(RPG.getMsg("notfound").get(0));
                return true;
            }

            p.teleport(((Player) sender).getLocation());
            p.sendMessage(RPG.getMsg("tp").get(0).replace("{player}", sender.getName()));
        }

        // TODO
        if(cmd.getName().equalsIgnoreCase("tpa"))
        {
            if (args.length < 1)
            {
                sender.sendMessage("§c/tpa <hrac>");
                return true;
            }
        }

        if(cmd.getName().equalsIgnoreCase("money"))
        {
            if(args.length == 0)
            {
                Hero h = Holder.players.get(sender.getName());

                if(h == null)
                {
                    sender.sendMessage(RPG.getMsg("notfound").get(0));
                    return true;
                }

                RPG.getMsg("money").forEach(m -> sender.sendMessage(m.replace("{money}", h.getGolds()+"")));
                return true;
            }
            else
            {
                if(!sender.hasPermission("rpg.money.other"))
                {
                    sender.sendMessage("§cNemas permissie na toto!");
                    return true;
                }

                Hero h = Holder.getPlayer(args[0]);

                if(h == null)
                {
                    sender.sendMessage(RPG.getMsg("notfound").get(0));
                    return true;
                }

                RPG.getMsg("moneyother").forEach(m -> sender.sendMessage(m.replace("{money}",
                        h.getGolds()+"").replace("{player}", args[0])));
                return true;
            }
        }

        if(cmd.getName().equalsIgnoreCase("crystals"))
        {
            if(args.length == 0)
            {
                Hero h = Holder.players.get(sender.getName());

                if(h == null)
                {
                    sender.sendMessage(RPG.getMsg("notfound").get(0));
                    return true;
                }

                RPG.getMsg("crystals").forEach(m -> sender.sendMessage(m.replace("{crystals}", h.getCrystals()+"")));
                return true;
            }
            else
            {
                if(!sender.hasPermission("rpg.crystals.other"))
                {
                    sender.sendMessage("§cNemas permissie na toto!");
                    return true;
                }

                Hero h = Holder.getPlayer(args[0]);

                if(h == null)
                {
                    sender.sendMessage(RPG.getMsg("notfound").get(0));
                    return true;
                }

                RPG.getMsg("crystalsother").forEach(m -> sender.sendMessage(m.replace("{crystals}",
                        h.getGolds()+"").replace("{player}", args[0])));
                return true;
            }
        }

        return true;
    }

    private void sendPM(Player from, String[] args)
    {
        Player to = Bukkit.getServer().getPlayerExact(args[0]);

        if(to == null)
        {
            from.sendMessage("§cHrac nie je online!");
            return;
        }

        List<String> msgl = new ArrayList<>();
        msgl.addAll(Arrays.asList(args));
        msgl.remove(0);
        String[] ss = new String[msgl.size()];
        sendPM(from, to, msgl.toArray(ss));
    }

    private void sendPM(Player from, Player to, String[] args)
    {
        String msg = String.join(" ", args);

        String fullmsg = Holder.prefixes.get("pm").replace("{from}", from.getName()).
                replace("{to}", to.getDisplayName()).replace("{msg}", msg);

        to.sendMessage(fullmsg);
        from.sendMessage(fullmsg);

        comm.put(to, from);
    }

    private void teleportPlayer(Player sender, String to)
    {
        Player p = Bukkit.getServer().getPlayer(to);

        if(p == null)
        {
            sender.sendMessage(RPG.getMsg("notfound").get(0));
            return;
        }

        sender.teleport(p.getLocation());
        sender.sendMessage(RPG.getMsg("tp").get(0).replace("{player}", p.getDisplayName()));
    }
}
