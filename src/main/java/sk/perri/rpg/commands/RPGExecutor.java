package sk.perri.rpg.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import sk.perri.rpg.Holder;
import sk.perri.rpg.RPG;
import sk.perri.rpg.classes.Hero;

public class RPGExecutor implements CommandExecutor
{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(args.length == 0 || !sender.isOp())
        {
            sender.sendMessage("§7§l-------------");
            sender.sendMessage("§a§lRPG §7by §2§lPerri");
            sender.sendMessage("§7§l-------------");
        }
        else if(args[0].equalsIgnoreCase("reload"))
        {
            RPG.instance.loadConfig();
            sender.sendMessage("§aConfigs reloaded!");
        }
        else if(args[0].equalsIgnoreCase("settempgroup"))
        {
            if(args.length != 3)
            {
                sender.sendMessage("§c/rpg settempgroup <player> <group>");
                return true;
            }

            Hero h = Holder.getPlayer(args[1]);

            if(h == null)
            {
                sender.sendMessage("§cPlayer not found!");
                return true;
            }

            h.setGroup(args[2]);
        }
        else
        {
            sender.sendMessage("§e/rpg reload - Reload all configs");
            sender.sendMessage("§e/rpg settempgroup <player> <group> - Set temporary group <group> to <player>");
        }

        return true;
    }
}
