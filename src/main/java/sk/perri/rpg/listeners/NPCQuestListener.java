package sk.perri.rpg.listeners;

import net.citizensnpcs.api.event.NPCLeftClickEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import sk.perri.rpg.Holder;
import sk.perri.rpg.classes.Hero;
import sk.perri.rpg.classes.Quest;
import sk.perri.rpg.inventories.Inventories;

import java.util.List;

public class NPCQuestListener implements Listener
{
    @EventHandler
    public void onNPCClick(NPCRightClickEvent event)
    {
        List<Quest> questy = Holder.getQuestsByOwner(event.getNPC().getName());

        if(questy.size() > 0)
        {
            Hero h = Holder.getPlayer(event.getClicker().getName());
            event.getClicker().openInventory(Inventories.questInventory(h, questy, event.getNPC().getName()));
        }
    }

    @EventHandler
    public void onNPCLeftClick(NPCLeftClickEvent event)
    {
        List<Quest> questy = Holder.getQuestsByOwner(event.getNPC().getName());

        if(questy.size() > 0)
        {
            Hero h = Holder.getPlayer(event.getClicker().getName());
            event.getClicker().openInventory(Inventories.questInventory(h, questy, event.getNPC().getName()));
        }
    }
}
