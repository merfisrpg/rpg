package sk.perri.rpg.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import sk.perri.rpg.Holder;
import sk.perri.rpg.classes.Hero;
import sk.perri.rpg.classes.Quest;

public class QuestListener implements Listener
{
    @EventHandler
    public void onMonsterSlain(EntityDamageByEntityEvent event)
    {
        if(!(event.getEntity() instanceof LivingEntity))
            return;

        if(!(event.getDamager() instanceof Player))
            return;

        if(((LivingEntity) event.getEntity()).getHealth() - event.getFinalDamage() <= 0 && event.getEntity().isCustomNameVisible())
        {
            Hero h = Holder.getPlayer(event.getDamager().getName());

            if(h == null)
                return;

            String ename = ChatColor.stripColor(event.getEntity().getCustomName()).toLowerCase();
            for(int qid : h.getOpenQuests().keySet())
            {
                Quest q = Holder.quests.get(qid);
                if(q.getType().contains("KILL") && q.getTarget().toLowerCase().contains(ename))
                {
                    if(h.getQuestStatus(qid) < 0)
                        continue;

                    h.setQuestStatus(qid, h.getQuestStatus(qid)+1);


                    if(h.getQuestStatus(qid) >= q.getAmount())
                    {
                        q.getEnd().forEach(m -> event.getDamager().sendMessage(m));
                        h.comleteQuest(qid);
                    }
                    else
                    {
                        event.getDamager().sendMessage(q.getShortdesc().get(0)+ " §b"+h.getQuestStatus(qid)+"/"+q.getAmount());
                    }

                    break;
                }
            }
        }
    }

    @EventHandler
    public void onQuestInvClick(InventoryClickEvent event)
    {
        if(event.getClickedInventory() == null || !event.getClickedInventory().getTitle().contains("- quest") ||
                event.getCurrentItem() == null)
            return;

        for(Quest q : Holder.quests.values())
        {
            if(q.getName().equalsIgnoreCase(ChatColor.stripColor(event.getCurrentItem().getItemMeta().getDisplayName())))
            {
                Holder.getPlayer(event.getWhoClicked().getName()).openQuest(q.getId());
                q.getLongdesc().forEach(m -> event.getWhoClicked().sendMessage(m));
                break;
            }
        }

        event.setCancelled(true);
    }
}