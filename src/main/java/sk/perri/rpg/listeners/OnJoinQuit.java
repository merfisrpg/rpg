package sk.perri.rpg.listeners;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.inventory.ItemStack;
import sk.perri.rpg.Holder;
import sk.perri.rpg.RPG;
import sk.perri.rpg.classes.Hero;
import sk.perri.rpg.inventories.Inventories;
import sk.perri.rpg.levels.Levels;

public class OnJoinQuit implements Listener
{
    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        Holder.players.put(event.getPlayer().getName(), new Hero(event.getPlayer()));
        event.setJoinMessage(RPG.getMsg("joinmsg").get(0).replace("{player}", event.getPlayer().getDisplayName()));

        /*if(Holder.players.get(event.getPlayer().getName()).getDemon().equalsIgnoreCase(""))
        {
            Bukkit.getScheduler().runTaskLater(RPG.instance, () -> RPG.instance.openStoryBook(event.getPlayer()), 20);
        }*/
        if(Holder.players.get(event.getPlayer().getName()).getClazz().equalsIgnoreCase(""))
            Bukkit.getScheduler().runTaskLater(RPG.instance, () -> event.getPlayer().openInventory(Inventories.classSelector()), 20);
        event.getPlayer().setLevel(Holder.players.get(event.getPlayer().getName()).getLevel());
        event.getPlayer().setExp(Math.min(1, Holder.players.get(event.getPlayer().getName()).getExp() /
                (float) Levels.getExpForLevel(Holder.players.get(event.getPlayer().getName()).getLevel())));

        final boolean[] hasWand = {false};

        event.getPlayer().getInventory().forEach(is ->
        {
            if(is != null && is.hasItemMeta() && is.getItemMeta().hasDisplayName() && is.getItemMeta().getDisplayName().contains(
                    Holder.items.get("palicka").getItemMeta().getDisplayName()))
            {
                hasWand[0] = true;
            }
        });

        if(!hasWand[0])
        {
            ItemStack hol = event.getPlayer().getInventory().getItem(0);
            event.getPlayer().getInventory().setItem(0, Holder.getNewItem(event.getPlayer(), "palicka"));
            if(hol != null)
                event.getPlayer().getInventory().addItem(hol);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event)
    {
        if(Holder.players.containsKey(event.getPlayer().getName()))
        {
            Holder.players.remove(event.getPlayer().getName()).saveToDB();
        }

        event.setQuitMessage(RPG.getMsg("quitmsg").get(0).replace("{player}", event.getPlayer().getDisplayName()));
    }

    @EventHandler
    public void onPing(ServerListPingEvent event)
    {
        event.setMotd(Holder.motd);
    }
}
