package sk.perri.rpg.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import sk.perri.rpg.Holder;

public class MonsterListener implements Listener
{
    @EventHandler
    public void onMonserKill(EntityDeathEvent event)
    {
        if(event.getEntity().isCustomNameVisible() && !(event.getEntity() instanceof Player))
        {
            if(Holder.monsters.containsKey(ChatColor.stripColor(event.getEntity().getCustomName()).toLowerCase()))
            {
                event.setDroppedExp(Holder.monsters.get(ChatColor.stripColor(event.getEntity().getCustomName()).toLowerCase()).getExps());
            }
        }
    }
}
