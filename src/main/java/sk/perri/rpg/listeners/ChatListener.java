package sk.perri.rpg.listeners;

import net.md_5.bungee.api.chat.*;
import org.bukkit.Bukkit;
import org.bukkit.Statistic;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import sk.perri.rpg.Holder;
import sk.perri.rpg.Utils;
import sk.perri.rpg.classes.Hero;
import sk.perri.rpg.levels.Levels;

public class ChatListener implements Listener
{
    @EventHandler
    public void onChat(AsyncPlayerChatEvent event)
    {
        int level = -1;
        Double lvlperc = null;
        Hero h = Holder.players.get(event.getPlayer().getName());
        String group = "default";

        if (h != null)
        {
            level = h.getLevel();
            lvlperc = Math.round(1000 * h.getExp() / (double) Levels.getExpForLevel(h.getLevel())) / 10.0;
            group = h.getGroup();
        }

        BaseComponent header = new TextComponent();
        TextComponent tcl = null;

        if (level > 0)
        {
            tcl = new TextComponent(Holder.prefixes.get("level").replace("{level}", level + "").
                    replace("{lvlcolor}", lvlColor(h.getLevel())));
            tcl.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(
                    Holder.prefixes.get(h.getGroup() + "lh").replace("{lvlperc}", lvlperc + "")).create()));

            header.addExtra(tcl);
        }

        TextComponent name = new TextComponent(new ComponentBuilder(Holder.prefixes.get(group+"p")+event.getPlayer().
                getDisplayName()).append(Holder.prefixes.get(group + "s")).create());

        // !!! HERE ADD HOVER PLACEHOLDERS !!!
        name.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                new ComponentBuilder(Holder.prefixes.get(group+"ph").
                        replace("{player}", event.getPlayer().getDisplayName()).
                        replace("{lvl}", h.getLevel()+"").
                        replace("{petname}", "-").
                        replace("{voted}", "0").
                        replace("{class}", Holder.clazzes.get(h.getClazz()).getName()).
                        replace("{playtime}", Utils.getPlayTime(event.getPlayer()))).create()));

        header.addExtra(name);
        header.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/pm "+event.getPlayer().getDisplayName()+" "));

        BaseComponent msg = new TextComponent(Holder.prefixes.get("delimeter")+Holder.prefixes.get(group+"m")+event.getMessage().replace(" ", " "+Holder.prefixes.get(group+"m")));

        BaseComponent fullmsg = new TextComponent();
        fullmsg.addExtra(header);
        fullmsg.addExtra(msg);

        Bukkit.getServer().spigot().broadcast(fullmsg);
        Bukkit.getConsoleSender().sendMessage(fullmsg.toPlainText());
        event.setCancelled(true);
    }

    private String lvlColor(int lvl)
    {
        int res = 100;
        for(Integer i : Holder.lvlColors.keySet())
        {
            if(lvl < i)
                res = Math.min(i, res);
        }

        return Holder.lvlColors.get(res);
    }
}
