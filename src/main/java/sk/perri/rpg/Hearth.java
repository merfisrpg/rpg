package sk.perri.rpg;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import sk.perri.rpg.area.Area;

public class Hearth
{
    int areaCheck = 100;
    int sbSwitch = Holder.boardSwitchTime;
    int sbUpdate = 600;
    public BukkitTask task;

    public Hearth()
    {
        task = Bukkit.getScheduler().runTaskTimer(RPG.instance, hearthBeat, 20, 1);
    }

    public Runnable hearthBeat = new BukkitRunnable()
    {
        @Override
        public void run()
        {
            areaCheck --;
            sbSwitch--;
            sbUpdate--;

            if(areaCheck <= 0)
            {
                Holder.players.forEach((p, h) ->
                {
                    if((h.getArea() != null && !h.getArea().isIn(h.getPlayer().getLocation(), false)) ||
                            h.getArea() == null)
                    {
                        boolean in = false;

                        for(Area a: Holder.areas.values())
                        {
                            if(a.isIn(h.getPlayer().getLocation(), false))
                            {
                                // Enter area
                                h.setArea(a);
                                if(a.showTitle())
                                    h.getPlayer().sendTitle(a.getTitle(), "", 10, 40, 10);

                                in = true;
                                break;
                            }
                        }

                        if(!in)
                            h.setArea(null);
                    }
                });

                areaCheck = 100;
            }

            if(sbSwitch <= 0)
            {
                //Bukkit.getLogger().info("SB SWITCH");
                Holder.players.values().forEach(h -> h.getSbManager().switchBoard());

                sbSwitch = Holder.boardSwitchTime;
            }

            if(sbUpdate <= 0)
            {
                Holder.players.values().forEach(h -> h.getSbManager().updateBoard());
                sbUpdate = 600;
            }
        }
    };
}
