package sk.perri.rpg.inventories;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemFactory
{
    public static ItemStack createItem(Material m, int amount, String name, boolean enchanted)
    {
        return createItem(m, amount, -1, name, null, enchanted);
    }

    public static ItemStack createItem(Material m, int amount, String name, List<String> lore, boolean enchanted)
    {
        return createItem(m, amount, -1, name, lore, enchanted);
    }

    public static ItemStack createItem(Material m, int amount, int data, String name, List<String> lore, boolean enchanted)
    {
        ItemStack is;

        if(data == -1)
            is = new ItemStack(m, amount);
        else
            is = new ItemStack(m, amount, (short) data);

        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        if(lore != null)
            im.setLore(lore);

        if(enchanted)
        {
            im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
            im.addEnchant(Enchantment.LUCK, 1, true);
        }

        is.setItemMeta(im);

        return is;
    }

    public static ItemStack createItem(ConfigurationSection cs)
    {
        ItemStack res;
        try
        {
            if(cs.isSet("data") && cs.getInt("data") < 0)
                res = new ItemStack(Material.matchMaterial(cs.getString("material")), 1, (byte) cs.getInt("data"));
            else
                res = new ItemStack(Material.matchMaterial(cs.getString("material")), 1);

            ItemMeta im = res.getItemMeta();

            im.setDisplayName(ChatColor.translateAlternateColorCodes('&', cs.getString("name")));

            List<String> trans = new ArrayList<>();
            cs.getStringList("lore").forEach(m -> trans.add(ChatColor.translateAlternateColorCodes('&', m)));

            im.setLore(trans);

            if(cs.isSet("enchants"))
            {
                cs.getConfigurationSection("enchants").getKeys(false).forEach(kk ->
                    im.addEnchant(Enchantment.getByName(kk), cs.getInt("enchants."+kk), true));
            }

            res.setItemMeta(im);
            return res;
        }
        catch (Exception e)
        {
            Bukkit.getLogger().warning("Unable to create item: "+e.toString());
        }
        return null;
    }
}
