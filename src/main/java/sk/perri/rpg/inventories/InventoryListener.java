package sk.perri.rpg.inventories;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import sk.perri.rpg.Holder;
import sk.perri.rpg.RPG;
import sk.perri.rpg.classes.Hero;
import sk.perri.rpg.inventories.Inventories;

public class InventoryListener implements Listener
{
    @EventHandler
    public void onInvClick(InventoryClickEvent event)
    {
        if(event.getClickedInventory() == null || event.getCurrentItem() == null)
            return;

        if(event.getClickedInventory().getTitle().contains(Inventories.classSelector().getTitle()))
        {
            String name;
            event.setCancelled(true);
            if(event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().hasDisplayName())
                    name = event.getCurrentItem().getItemMeta().getDisplayName();
            else
                return;

            Hero h = Holder.players.get(event.getWhoClicked().getName());

            if(h == null)
            {
                return;
            }

            Holder.clazzes.forEach((n, c) ->
            {
                if(c.getItem().getItemMeta().getDisplayName().contains(name))
                {
                    h.setClazz(n);
                }
            });

            h.saveToDB();

            Bukkit.getScheduler().runTaskLater(RPG.instance, () -> event.getWhoClicked().closeInventory(), 2);
        }
    }

    @EventHandler
    public void onInvClose(InventoryCloseEvent event)
    {
        if(event.getInventory() == null)
            return;

        if(event.getInventory().getTitle().contains(Inventories.classSelector().getTitle()) && Holder.getPlayer(event.getPlayer().getName()).getClazz().equalsIgnoreCase(""))
            Bukkit.getScheduler().runTaskLater(RPG.instance, () -> event.getPlayer().openInventory(Inventories.classSelector()), 2);
    }

    @EventHandler
    public void onFirstBookClose(PlayerMoveEvent event)
    {
        if(Holder.players.containsKey(event.getPlayer().getName()) &&
                Holder.players.get(event.getPlayer().getName()).getTut() == 1)
            RPG.instance.openGameBook(event.getPlayer());
    }

    @EventHandler
    public void onItemDrop(PlayerDropItemEvent event)
    {
        if(event.getItemDrop().getItemStack().hasItemMeta() &&
                event.getItemDrop().getItemStack().getItemMeta().hasDisplayName() &&
                event.getItemDrop().getItemStack().getItemMeta().getDisplayName().contains(
                        Holder.items.get("palicka").getItemMeta().getDisplayName()))
        {
            event.setCancelled(true);
        }
    }
}
