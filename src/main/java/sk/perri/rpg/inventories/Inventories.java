package sk.perri.rpg.inventories;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import sk.perri.rpg.Holder;
import sk.perri.rpg.RPG;
import sk.perri.rpg.classes.Hero;
import sk.perri.rpg.classes.Quest;

import java.util.HashMap;
import java.util.List;

public class Inventories
{
    private Inventories() {}

    public static Inventory classSelector()
    {
        Inventory inv = Bukkit.createInventory(null, 9, "§c§lVYBER CLASSY");

        /*inv.setItem(2, ItemFactory.createItem(Material.GOLD_SWORD, 1, "§c§lBOJOVNIK", RPG.getMsg("dmglore"), false));
        inv.setItem(4, ItemFactory.createItem(Material.BOW, 1, "§6§lLOVEC", RPG.getMsg("hunterlore"), false));
        inv.setItem(6, ItemFactory.createItem(Material.BLAZE_ROD, 1, "§3§lMAG", RPG.getMsg("magelore"), false));*/
        inv.setItem(2, Holder.clazzes.get("A").getItem());
        inv.setItem(4, Holder.clazzes.get("B").getItem());
        inv.setItem(6, Holder.clazzes.get("C").getItem());
        return inv;
    }

    public static Inventory questInventory(Hero hero, List<Quest> quests, String owner)
    {
        Inventory inv = Bukkit.createInventory(null, quests.size() <= 18 ? 27 : 54, owner+" - questy");

        quests.forEach(q -> inv.addItem(q.getQuestItem(hero != null ? hero.getQuestFinalStatus(q.getId()) : 0)));

        return inv;
    }

    public static HashMap<String, ItemStack[]> plrInv = new HashMap<>();
}
